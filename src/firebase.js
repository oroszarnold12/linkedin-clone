import firebase from 'firebase';

const firebaseConfig = {
  apiKey: 'AIzaSyBCmYtEUQHL4AG8_mzhCH4sWAcr0_4T3u8',
  authDomain: 'linkedin-clone-58e33.firebaseapp.com',
  projectId: 'linkedin-clone-58e33',
  storageBucket: 'linkedin-clone-58e33.appspot.com',
  messagingSenderId: '68386594763',
  appId: '1:68386594763:web:23cc5ddbc4822e161da04e',
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

const db = firebaseApp.firestore();

const auth = firebase.auth();

export { db, auth };
