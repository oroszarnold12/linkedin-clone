import { FiberManualRecord, Info } from '@material-ui/icons';
import './Widgets.css';

function Widgets() {
  const newsArticle = (heading, subtitle) => (
    <div className="widgets-article">
      <div className="widgets-article-left">
        <FiberManualRecord />
      </div>

      <div className="widgets-article-right">
        <h4>{heading}</h4>
        <p>{subtitle}</p>
      </div>
    </div>
  );

  return (
    <div className="widgets">
      <div className="widgets-header">
        <h2>LinkedIn News</h2>
        <Info />
      </div>

      {newsArticle('PAPA React is back', 'Top news - 9099 readers')}
      {newsArticle('PAPA React is back', 'Top news - 9099 readers')}
      {newsArticle('PAPA React is back', 'Top news - 9099 readers')}
      {newsArticle('PAPA React is back', 'Top news - 9099 readers')}
      {newsArticle('PAPA React is back', 'Top news - 9099 readers')}
    </div>
  );
}

export default Widgets;
