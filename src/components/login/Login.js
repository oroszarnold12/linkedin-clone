import { useRef } from 'react';
import './Login.css';
import { auth } from '../../firebase';
import { useDispatch } from 'react-redux';
import { login } from '../../features/userSlice';

function Login() {
  const nameRef = useRef(null);
  const emailRef = useRef(null);
  const passwordRef = useRef(null);
  const photoUrlRef = useRef(null);

  const dispatch = useDispatch();

  const register = () => {
    if (!nameRef.current.value) {
      return alert('Please enter a full name!');
    }

    auth
      .createUserWithEmailAndPassword(
        emailRef.current.value,
        passwordRef.current.value
      )
      .then((userAuth) => {
        userAuth.user
          .updateProfile({
            displayName: nameRef.current.value,
            photoURL: photoUrlRef.current.value,
          })
          .then(() => {
            dispatch(
              login({
                email: userAuth.user.email,
                uid: userAuth.user.uid,
                displayName: nameRef.current.value,
                photoURL: photoUrlRef.current.value,
              })
            );
          });
      })
      .catch((error) => alert(error.message));
  };

  const loginToApp = (e) => {
    e.preventDefault();

    auth
      .signInWithEmailAndPassword(
        emailRef.current.value,
        passwordRef.current.value
      )
      .then((userAuth) => {
        dispatch(
          login({
            email: userAuth.user.email,
            uid: userAuth.user.uid,
            displayName: userAuth.user.displayName,
            photoURL: userAuth.user.photoURL,
          })
        );
      })
      .catch((error) => alert(error));
  };

  return (
    <div className="login">
      <img
        src="https://news.hitb.org/sites/default/files/styles/large/public/field/image/500px-LinkedIn_Logo.svg__1.png?itok=q_lR0Vks"
        alt=""
      ></img>

      <form>
        <input
          type="text"
          placeholder="Full name (required if registering)"
          ref={nameRef}
        />

        <input
          placeholder="Profile pic URL (optional)"
          type="text"
          ref={photoUrlRef}
        />

        <input placeholder="Email" type="email" ref={emailRef} />

        <input placeholder="Password" type="password" ref={passwordRef} />

        <button type="submit" onClick={loginToApp}>
          Sign In
        </button>
      </form>

      <p>
        Not a member?{' '}
        <span onClick={register} className="login-register">
          Register Now
        </span>
      </p>
    </div>
  );
}

export default Login;
