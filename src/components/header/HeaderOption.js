import { Avatar } from '@material-ui/core';
import './HeaderOption.css';

function HeaderOption({ avatar, Icon, title, onClick, character }) {
  return (
    <div onClick={onClick} className="header-option">
      {Icon && <Icon className="header-option-icon" />}
      {avatar && (
        <Avatar className="header-option-icon" src={avatar}>
          {character}
        </Avatar>
      )}
      <h3 className="header-option-title">{title}</h3>
    </div>
  );
}

export default HeaderOption;
